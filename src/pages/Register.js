import { useEffect, useState } from "react"
import { Form, Row, Col, Container, Button } from "react-bootstrap"
import { useNavigate } from "react-router-dom"

export default function Register () {
    let navigate = useNavigate()
    const [fN, setFN] = useState ("")
    const [lN, setLN] = useState ("")
    const [email, setEmail] = useState ("")
    const [pw, setPw] = useState ("")
    const [vpw, setVpw] = useState ("")
    const [isDisabled, setIsDisabled] = useState(true)

    useEffect(() => {
        if((fN !== "" && lN !== "" && email !== "" && pw !== "" && vpw !== "") 
        && (pw === vpw)){
            setIsDisabled(false)
        }else{
            setIsDisabled(true)
        }        
    }, [fN,lN,email,pw,vpw])
 
    const registerUser = (e) => {
        
        fetch(`http://localhost:4005/api/users/email-exists`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
            })
        }).then(response => response.json())
        .then(response => {
            if(!response){
                fetch(`http://localhost:4005/api/users/register`, {
					method: "POST",
					headers:{
						"Content-Type": "application/json",
					},
					body: JSON.stringify({
						//user input
						firstName: fN,
						lastName: lN,
						email: email,
						password: pw
					})
				})
				.then(result => result.json())
				.then(result => {
					// console.log(result)	//boolean
					if(result){
						alert('User successfully registered!')
                        navigate('/login')
					} else {
						alert(`Please try again`)
					}
				})

            }else{
                alert (`User already exists`)
            }
        })
    }

    return(
        <Container className="m-5">
            <h3 className="text-center">Register</h3>
            <Row className="justify-content-center">
                <Col xs={12} md={6}>
                    <Form onSubmit={(e) => registerUser(e) }>
                        <Form.Group className="mb-3">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control 
                            type="text" 
                            value={fN}
                            onChange={(e) => setFN(e.target.value)}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control type="text" value={lN}
                            onChange={(e) => setLN(e.target.value)}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control 
                            type="text" 
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                            type="password" 
                            value={pw}
                            onChange={(e) => setPw(e.target.value)}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Verify Password</Form.Label>
                            <Form.Control 
                            type="password" 
                            value={vpw}
                            onChange={(e) => setVpw(e.target.value)}
                            />
                        </Form.Group>
                        <Button 
                        variant="info" 
                        type="submit"
                        disabled={isDisabled}
                        >
                        Submit</Button>
                    </Form>
                </Col>
            </Row>
        </Container>

    )
}